#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created on 2018/6/27
import re
import sys
import time

import requests
from lxml import etree

from . import config
from dbConnection import myRedis
from dbConnection.MysqlConn import Mysql





def downloadTask():
    url = "https://movie.douban.com/cinema/nowplaying/changsha/"
    response = requests.get(url=url, headers=config.header)
    selector = etree.HTML(response.text)
    tmp_list = selector.xpath('//div[@class="mod-bd"]/ul[@class="lists"]')  # 这里使用class属性来定位 电影列表
    save_to_db(tmp_list[0])  # 数组第一项即为正在热映的电影


def save_to_db(movie_data):
    # 取上次更新的记录ID
    last_id = myRedis.get("last_id")
    mysql = Mysql()
    tmp_id = ""
    # 更新正在上映的数据(存在则更新，否则插入)
    for index, item in enumerate(movie_data):
        mid = item.xpath('./@id')[0]  # 电影ID
        tmp_id = tmp_id + mid + ","
        score = item.xpath('./@data-score')[0]  # 评分
        category = item.xpath('./@data-category')[0]  # 电影状态
        status = 1 if category == "nowplaying" else 2
        sel_sql = "SELECT id,status FROM movie_list WHERE id = %s " % mid
        sel_res = mysql.getAll(sql=sel_sql, param=None)
        if sel_res is None:
            title = item.xpath('./@data-title')[0]  # 电影名
            release = item.xpath('./@data-release')[0]  # 上映年份
            duration = re.findall(r'(\w*[0-9]+)\w*', item.xpath('./@data-duration')[0])[0]  # 时长
            region = item.xpath('./@data-region')[0]  # 区域
            director = item.xpath('./@data-director')[0]  # 导演
            actors = item.xpath('./@data-actors')[0]  # 主演
            detail = item.xpath('./ul/li/a/@href')[0]  # 详情页地址
            img = item.xpath('./ul/li/a/img/@src')[0]  # 封面图地址
            insert_sql = "INSERT INTO `movie_list` (`id`, `title`, `img`, `release`, `score`, `duration`, `region`, `director`, `actors`,detail, `status`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');" % (
                mid, title, img, release, score, duration, region, director, actors, detail, status)
            mysql.insertOne(sql=insert_sql, value=None)
        else:
            status = status if mid in last_id else 0
            update_sql = "UPDATE `movie_list` SET score = '%s',status ='%s' WHERE id = '%s' " % (score, status, mid)
            mysql.update(sql=update_sql, param=None)
    mysql.dispose()
    tmp_id = tmp_id[0:len(tmp_id) - 1]
    myRedis.set("last_id", tmp_id)


def getIntroduce():
    mysql = Mysql()
    result = mysql.getAll(sql="SELECT id FROM movie_list where report is NULL ", param=None)
    for r in result:
        cid = r["id"]
        url = "https://movie.douban.com/subject/%s/?from=playing_poster" % cid
        response = requests.get(url=url, headers=config.header)
        selector = etree.HTML(response.text)
        tmp = selector.xpath('//div[@id="link-report"]/span/text()')
        text = ""
        for t in tmp:
            text += str(t).replace(" ", "")
        update_sql = "UPDATE movie_list SET report ='%s' where id = '%s' " % (text, cid)
        mysql.update(sql=update_sql, param=None)
    mysql.dispose()


while True:
    downloadTask()
    getIntroduce()
    time.sleep(60 * 60 * 24)
