#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created on 2018/6/27


class movie:

    def __init__(self, title, release, score, trailer, ticket, duration, region, director, actors, rater):
        self.title = title  # 名字
        self.release = release  # 年份
        self.score = score  # 评分
        self.trailer = trailer  # 预告片链接
        self.ticket = ticket  # 购票链接
        self.duration = duration  # 时长
        self.region = region  # 区域
        self.director = director  # 导演
        self.actors = actors  # 主演
        self.rater = rater  # 评价人数
