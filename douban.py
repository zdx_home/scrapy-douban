#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created on 2018/6/28
from app import create_app

if __name__ == '__main__':
    app = create_app()
    app.run(host="127.0.0.1", port=5001)
