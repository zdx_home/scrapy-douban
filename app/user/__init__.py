#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created on 2018/6/28 
from flask import Blueprint

user = Blueprint(import_name="user", name=__name__)

from . import movie
