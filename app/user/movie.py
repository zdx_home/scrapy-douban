#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created on 2018/6/28 
import sys

from flask import redirect, render_template

from . import user

reload(sys)
sys.setdefaultencoding('utf8')


@user.route("/")
def home():
    return redirect("/move/list.html")


@user.route("/move/list.html", methods=["GET"])
def move_list():
    return render_template("user/movie.html")
