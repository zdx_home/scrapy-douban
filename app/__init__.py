#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created on 2018/6/28 
import sys

from flask import Flask

reload(sys)
sys.setdefaultencoding('utf8')


def create_app():
    app = Flask(__name__)

    from .user import user as user_blueprint
    app.register_blueprint(blueprint=user_blueprint)

    return app
